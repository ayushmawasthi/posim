import React from 'react';
import { ActivityIndicator } from 'react-native'
//import { StyleSheet, Text, View } from 'react-native';
import { UtilityThemeProvider,  Box, Text } from 'react-native-design-utility';
import Navigation from './src/screens';
import { images } from './src/constant/images';
import { cacheImages } from './src/utils/cacheimages';
import {theme} from './src/constant/theme'

export default class App extends React.Component {
  state={
    isReady:false
  };
  componentDidMount(){
    this.cacheAssets();

  }
  cacheAssets= async() =>{
    const imageAssets = cacheImages(Object.values(images));
    await Promise.all([...imageAssets]);
    this.setState({isReady:true});

  }
  render(){
    if(!this.state.isReady){
      return (
        <Box f={1} center bg='#fff'>
          <ActivityIndicator size='large'/>

        </Box>
      )
    }
  return (
    <UtilityThemeProvider theme={theme}>
    <Navigation/>
    </UtilityThemeProvider>
  );
}}


