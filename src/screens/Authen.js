import React,{Component} from 'react';
import { Text} from 'react-native-design-utility';
import {AppRegistry,Image, View,StyleSheet, TouchableOpacity} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import LoginScreen from './LoginScreen';
import SignScreen from './SignScreen';

class Authen extends Component{

    state={};
    
    render(){
        return(
            <View style={styles.container}>
                <Image style={styles.imgwel} source={require('../../assets/img/icon1.png')} />
                <Image style={styles.imgwel} source={require('../../assets/img/POSIM.png')} />
            
             <TouchableOpacity style={[styles.buttonContainer, styles.signupButton]} onPress = { this.props.navigation.navigate('test1') }>
             <Text style={styles.signUpText}>Sign up</Text>
           </TouchableOpacity>
           <TouchableOpacity style={[styles.buttonContainer, styles.signupButton]} onPress = { this.props.navigation.navigate('test') }>
             <Text style={styles.signUpText}>Log in</Text>
           </TouchableOpacity>
           </View>
            
        );

    }
}


const styles=StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:150,
        
        
    },
    buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
      },
      signupButton: {
        backgroundColor: "#1E94E8",
      },
      signUpText: {
        color: 'white',
      }

});
export default createStackNavigator({
    home:Authen,
    test:LoginScreen,
    test1:SignScreen,
})