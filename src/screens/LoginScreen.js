import React,{Component} from 'react';
import {Box, Text } from 'react-native-design-utility';
import { View, Image,Button, StyleSheet, TextInput, TouchableOpacity, ImageBackground } from 'react-native';

class LoginScreen extends Component{
    state={};
    static navigationOptions =
    {
     title: 'LoginActivity',
  } ;
    render(){
        return(
            <View style={styles.container}>
                <Image style={styles.imgwel} source={require('../../assets/img/welcome.png')} />
                <View style={{marginBottom:16}}>
                    
                    <Text style={styles.labelUser} >Username</Text>
                    <TextInput placeholder='Enter your email' style={styles.inputUser} source={require('../../assets/img/loginbg.png')} />
                   
                </View>
                <View style={{marginBottom:16}}>
                    
                    <Text style={styles.labelUser} >Password</Text>
                    <TextInput secureTextEntry={true} style={styles.inputUser} source={require('../../assets/img/loginbg.png')} />
                   
                </View>
                <View style={{marginBottom:16}}>
                <TouchableOpacity>
                    
                    <ImageBackground source={require('../../assets/img/loginbut.png')} style={styles.image}>
                    <Image source={require('../../assets/img/loginbutt.png')} style={styles.log}/>
                    </ImageBackground>
                    
                    </TouchableOpacity>
                </View>
            </View>
            
        );

    }
}
const styles=StyleSheet.create({
    container:{
        marginTop:30,
        marginLeft:24,
        marginRight: 24,
        marginBottom: 70,
        
        
    },
    imgwel:{
        width:206,
        height:27,
        marginLeft: 80,
        marginTop: 70,
    },
    labelUser:{
        fontSize:20,
        color: '#414E93',
        marginBottom:8,
    },
    inputUser:{
        width: 280,
        height: 45,
        borderColor: '#43519D',
        backgroundColor:'#03C8DD',
        borderRadius:8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        elevation: 1,
        color: '#fff',
        
    },
    image: {
        
        width:300,
        height:300,
        justifyContent: "center",
        borderRadius:8,
      },
    log:{
        alignContent:'center',
    },
   
});

export default LoginScreen;