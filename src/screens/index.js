
import { createStackNavigator , createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import React,{Component} from 'react';

const Authnavigator = createStackNavigator(
    {
        Login:{
            getScreen:()=>require('./Authen').default,
        },
    },
    {
        navigationOptions:{
            header:null,
        },
    },
);
const Tabnavigator = createBottomTabNavigator(
    {
        Home:{
            getScreen:()=>require('./HomeScreen').default,
        },
    }
);
const MainNavigator=createStackNavigator(
    {
        Tab: Tabnavigator,
    }
);
const AppNavigator=createSwitchNavigator(
    {
        Splash:{
            getScreen:()=>require('./SplashScreen').default,
        },
        Auth: Authnavigator,
        Main: MainNavigator,
    },
    {
        initializeRouteName: 'Splash'
    },
);
class Navigation extends Component{
    state={};
    render(){
        return <AppNavigator/>;
    }
}
export default Navigation;